class SQRT:

    def __init__(self, value):
        self._value = value
        self._result = self._sqrt_iter(1.0)

    @property
    def result(self):
        return self._result

    def _sqrt_iter(self, value):
        '''Итерация получения корня'''
        if self._check(value):
            return value

        return self._sqrt_iter(self._improve(value))

    def _improve(self, value):
        '''Изменение значения приближения'''
        return (value + self._value / value) / 2

    def _check(self, value):
        '''Проверка точности числа'''
        if abs(value**2 - self._value) < 0.001:
            return 1
        return 0
