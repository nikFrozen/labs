from sqrt import SQRT

if __name__ == '__main__':
    x = int(input('Введите значение: '))
    print(f"Корень квадратный = {SQRT(x).result}")
