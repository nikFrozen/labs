import argparse

from poly import Poly


class Program:

    def __init__(self):
        parser = argparse.ArgumentParser(description='Лабораторная №3')
        parser.add_argument('--poly=', action="store", required=True,
                            dest="poly", help="Список значение, через запятую")

        self.args = parser.parse_args()

    def _get_input_values(self):
        '''Получение введенных значений'''
        try:
            return [float(val) for val in self.args.poly.split(',')]
        except Exception:
            print("Input Error")
            exit()

    def get_poly_result(self):
        '''Получение результата выполнения полинома'''
        values = self._get_input_values()
        poly = Poly(values)
        return poly.get_result()
